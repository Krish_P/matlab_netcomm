"""Example program to show how to read a multi-channel time series from LSL."""

from pylsl import StreamInlet, resolve_stream


def TSLOreceiver():
    # first resolve an TSLO stream on the lab network
    print("looking for an TSLO stream...")
    streams = resolve_stream('type', 'TSLO')
    
    # create a new inlet to read from the stream
    inlet = StreamInlet(streams[0])
    
    repeat=True
    while repeat:
        # get a new sample (you can also omit the timestamp part if you're not
        # interested in it)
        sample, timestamp = inlet.pull_sample()
        print(timestamp, sample)
        if len(sample)>0:
            repeat=False
            inlet.close_stream()
    return (sample)
        
