"""Example program to demonstrate how to send a multi-channel time series to
LSL."""

import time
#from random import random as rand
from pylsl import StreamInfo, StreamOutlet

def TSLOsender(SubjectID,SessionNum,TrialNum,RecordingDur):
    # first create a new stream info (here we set the name to BioSemi,
    # the content-type to EEG, 8 channels, 100 Hz, and float-valued data) The
    # last value would be the serial number of the device or some other more or
    # less locally unique identifier for the stream as far as available (you
    # could also omit it but interrupted connections wouldn't auto-recover)
    #KSP: params:Format [SubjectID,SessionNum,TrialNum,RecordingDuration]
    params=[SubjectID,SessionNum,TrialNum,RecordingDur]
    msg=('sending params...')
    info = StreamInfo('Clight', 'TSLO', 4, 100, 'float32', 'myuid34234')
    
    # next make an outlet
    outlet = StreamOutlet(info)
            
    counter=0
    
    repeat=True
    while repeat:
        if counter>1:
            repeat=False
        if outlet.have_consumers()==False:
            continue

        mysample = [params[0],params[1],params[2],params[3]]
        # now send it and wait for a bit
        outlet.push_sample(mysample)
        counter+=1
        time.sleep(0.01)
