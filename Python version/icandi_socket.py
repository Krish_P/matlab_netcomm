import socket
import time

#from SendDataFunction import TSLOsender

TCP_IP = '127.0.0.1'
TCP_PORT = 1300

print("waiting for stimulus computer...")
subject_id=1
session_num=1
trial_num=1
video_length=2

#data_transfer=TSLOsender(subject_id,session_num,trial_num,video_length)
#del data_transfer
print("Connected. Talking to icandi..")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

mess1='VL#%.2f#'%(video_length)+'\0\0\0\0\0\0\0\0'
#mess2='GRVIDT#S0%.f_S%.f_T%002d#'%(subject_id,session_num,trial_num)
mess2='GRVIDT#test2#'+'\0'*60

mess0='UpdatePower#0#1#'+'\0\0\0\0\0\0\0\0'
m0=bytearray()
m0.extend(map(ord,mess0))
s.send(m0)

time.sleep(0.100)

m1=bytearray()
m1.extend(map(ord,mess1))

m2=bytearray()
m2.extend(map(ord,mess2))

s.send(m1)
time.sleep(0.01)
s.send(m0)
time.sleep(0.01)
s.send(m2)

#mess1='VL#%.2f#'%(2)
#m1=bytearray()
#m1.extend(map(ord,mess1))
#s.send(m1)
#time.sleep(0.005)
#s.send(m2)

#time.sleep(0.02)
#s.send(m0)

print("Sleeping")

#time.sleep(0.05)
time.sleep(2.0)

#s.send(m0)

print("About to close...")

s.close()

print("Closed socket")

time.sleep(3)
