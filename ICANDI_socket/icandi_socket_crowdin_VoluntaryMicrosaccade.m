% Purpose: Is to control experiments from the TSLO machine by sending
% messages to ICANDI and also to the stimulus computer using
% labstreaminglayer(LSL) libraries

clc;clear all;close all;
%% Experiment Parameters
% we load the audio file to indicate the completion of video recording
[y,Fs]=audioread('breep.wav');
% another one to indicate the end of the sequence
[y1,Fs1]=audioread('ding.wav');
% We would also have a toggle for running custom trial sequences
custom_trial=false;
% we set the no microsaccade condition
microsaccade_cond_blocked=false;
if custom_trial==false
    % Here we set the experimental conditions and the retinal video recording
    % parameters prior to the start of the experiment
    % Note: For spacing condition: Setting it to 1 which is also the default
    % would result in testing all flanker spacings otherwise when set to 0
    % would test the one that is specified locally on the stimulus machine
    prompt = {'SubjectID:','Session Number:','Num repeats:','Video Duration',...
               'Trial start counter:','No Microsaccade:','Anisotropy [0:Both;1:Radial;2:Tangential]:',...
               'Oblique cond [0: Horizontal; 1: Oblique location]:'};
    title = 'Experimental Protocol';
    dims = [1 35];
    definput = {'3','1','8','2','1','0','0','0'};
    response = inputdlg(prompt,title,dims,definput);
    ecc_cond=0.23; % We only test at this eccentricity    
else
    % We manually enter the trials that were found to have issues during
    % analysis
    [file,path,indx] = uigetfile('Z:\TSLO_Experiment\CrowdingStream\RepeatSequenceMatFiles');
    repeatFile=load([path,file]);repeatTrials=repeatFile.repeatTrials;
end
%% Main experiment
VideoParams.vidprefix='Test';
VideoParams.videofolder = 'no video folder';
if custom_trial==false
    VideoParams.videodur = str2num(response{4});
    % Here we use the response from the user to set the experimental parameters
    % some of which are sent across the stimulus computer through LSL
    % The following contain subject info
    subject_id=str2num(response{1});
    session_num=str2num(response{2});
    num_repeats=str2num(response{3});
    % We also get the trial start counter from the dialog box
    trial_start_counter=str2num(response{5});
    % We get the microsaccade condition
    if microsaccade_cond_blocked==false
        no_microsacc_cond=str2num(response{6});
        no_microsacc_sequence=[1];
    % we would shuffle the order of no microsaccade and microsaccade
    % conditions. But, we would have 5 microsaccade conditions and 1 no
    % microsaccade condition
    else
        no_microsacc_conds=[0,1];no_microsacc_conds=Shuffle(no_microsacc_conds);
        no_microsacc_sequence=[];
        if no_microsacc_conds(1)==0
            no_microsacc_sequence=[no_microsacc_sequence,repmat(0,1,5),1];
        elseif no_microsacc_conds(1)==1
            no_microsacc_sequence=[no_microsacc_sequence,1,repmat(0,1,5)];
        end
    end
    % we get radial-tangential anisotropy condition
    anisotropy_cond=str2num(response{7});
    % we set the meridian based on input
    if anisotropy_cond==1
        meridian='radial';
    elseif anisotropy_cond==2
        meridian='tangential';
    end
    % we set whether or not the stimulus should be presented at an oblique
    % location
    oblique_cond=str2num(response{8});
end

%This initiates the loop that initiates video recording on ICANDI and marks
%the start of the experiment
% We have two conditions here:
% 1. Normal trial sequence
% 2. Custom trial sequence
if custom_trial==false
    eccentricity=ecc_cond;
    video_length=VideoParams.videodur;
    f = waitbar(0,'1/96','Name','Block Progress');
    for no_microsacc_idx=1:length(no_microsacc_sequence)
        % we get the trial start number based on where the previous one
        % stopped
        if exist('trialStartNumNextBlock','var')
            trial_start_counter=trialStartNumNextBlock;
        else
            %do nothing
        end
        % we set the microsaccade condition
        no_microsacc_cond=no_microsacc_sequence(no_microsacc_idx);
        disp(no_microsacc_idx);
        % MESSAGE SEND:
        TSLOsender(anisotropy_cond,no_microsacc_cond,num_repeats,subject_id...
                    ,'Sending experiment Parameters...')
        TSLOsender(oblique_cond,anisotropy_cond,0,0,'Sending stimulus parameter')
        TSLOsender(subject_id,session_num,trial_start_counter,video_length...
                    ,'Sending subject info...');
        validTrialCounter=1;
        disp(validTrialCounter);
        while true
            % we get the flanker spacing condition from the display PC        
            stimulus_machine_packet=TSLOreceiver('Receiving info from stimulus machine...');
            % we format the spacing value as a string
            spacing_value=num2str(stimulus_machine_packet(1));
            if strcmp(spacing_value,'1.5')
                spacing_value='1.50';
            elseif strcmp(spacing_value,'1.2')
                spacing_value='1.20';
            elseif strcmp(spacing_value,'2.5')
                spacing_value='2.50';
            end
            trial_num=num2str(stimulus_machine_packet(2));
            no_microsacc_cond=(stimulus_machine_packet(3));
            SOA_value=num2str(stimulus_machine_packet(4));
            if strcmp(SOA_value,'0.05')
                SOA_value='0.050';
            end
            target_congruency_cond=(stimulus_machine_packet(5));
            disp(['Trial no.: ',trial_num,';','spacing:',spacing_value]);
            SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
            mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
            mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
            mess3= ['X#''dummy''#']; % This starts stabilization
            mess4=['Z#''dummy''#']; % This stops  video recording

            netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
            pause(0.001);
            netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
            pause(0.001);
    %         netcomm('write',SYSPARAMS.netcommobj,int8(mess3));
    %         pause(0.001);
            % Get output filenames for video and stabilization files
            timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');

            if no_microsacc_cond==1 && anisotropy_cond==0 && target_congruency_cond==1
                trial_cond_string=['_valid_',"NoMicrosaccade_Video_"];
            elseif no_microsacc_cond==1 && anisotropy_cond==1 && target_congruency_cond==1
                % also includes meridian info
                trial_cond_string=[meridian,'_valid_','NoMicrosaccade_Video'];
            elseif no_microsacc_cond==0 && anisotropy_cond==1 && target_congruency_cond==1
                % also includes meridian info
                trial_cond_string=[meridian,'_valid_','Video'];
            elseif no_microsacc_cond==0 && anisotropy_cond==0 && target_congruency_cond==0 %invalid trials
                trial_cond_string=['_invalid_','Video'];
            elseif no_microsacc_cond==0 && anisotropy_cond==0 && target_congruency_cond==1 % valid trials
                trial_cond_string=['_valid_','Video'];
            elseif no_microsacc_cond==1 && anisotropy_cond==0 && target_congruency_cond==0 %invalid trials
                trial_cond_string=['_invalid_','NoMicrosaccade_Video'];
            elseif no_microsacc_cond==1 && anisotropy_cond==1  && target_congruency_cond==0 % valid trials
               trial_cond_string=[meridian,'_invalid_','NoMicrosaccade_Video'];
            elseif no_microsacc_cond==0 && anisotropy_cond==1  && target_congruency_cond==0 % valid trials
               trial_cond_string=[meridian,'_invalid_','Microsaccade_Video'];
            elseif no_microsacc_cond==2 && anisotropy_cond==0 && target_congruency_cond==1
                trial_cond_string=['_valid_',"Neutral_Video_"];
            end
            % We use the info from earlier to name the file accordingly
            videoFilename=['S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,trial_cond_string,'_',timeStamp,'.avi'];
            stabCSV=[videoFilename(1:end-4),'.csv'];
            stabFilename=[videoFilename(1:end-4),'_stabilized','.avi'];
            videoFolder='D:\Video_Folder';
            %Filepaths
            videoPath=[videoFolder,'\',videoFilename];
            stabCSVPath=[videoFolder,'\',stabCSV];
            stabPath=[videoFolder,'\',stabFilename];
            % Start video recording 
            if no_microsacc_cond==1 && anisotropy_cond==0 && oblique_cond==0 && target_congruency_cond==0
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_invalid','_NoMicrosaccade_Video','_',timeStamp]));
            elseif no_microsacc_cond==1 && anisotropy_cond==0 && oblique_cond==0 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_valid','_NoMicrosaccade_Video','_',timeStamp]));
            elseif no_microsacc_cond==0 && anisotropy_cond==0 && oblique_cond==0 && target_congruency_cond==0
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_invalid','_Microsaccade_Video','_',timeStamp]));
            elseif no_microsacc_cond==0 && anisotropy_cond==0 && oblique_cond==0 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_valid','_Microsaccade_Video','_',timeStamp]));
            elseif no_microsacc_cond==2 && anisotropy_cond==0 && oblique_cond==0 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_valid','_Neutral_Video','_',timeStamp]));
            elseif no_microsacc_cond==0 && anisotropy_cond==0 && oblique_cond==0 && target_congruency_cond==1
                    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                        num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                        spacing_value,'_SOA_',SOA_value,'_valid','_Video','_',timeStamp]));
            elseif no_microsacc_cond==1 && anisotropy_cond~=0 && oblique_cond==0 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_',meridian,'_valid','_NoMicrosaccade_Video','_',timeStamp]));
            elseif no_microsacc_cond==0 && anisotropy_cond~=0 && oblique_cond==0 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_',meridian,'_valid','_Video','_',timeStamp]));
            %oblique condition
            elseif no_microsacc_cond==1 && anisotropy_cond~=0 && oblique_cond==1 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_',meridian,'_oblique','_valid','_NoMicrosaccade_Video','_',timeStamp]));
            elseif no_microsacc_cond==0 && anisotropy_cond~=0 && oblique_cond==1 && target_congruency_cond==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_SOA_',SOA_value,'_',meridian,'_oblique','_valid','_Video','_',timeStamp]));
            end
            pause(0.001);  

            %We receive a message regarding whether or not this trial is
            %the last one from the sequence
            endTrialPacket=TSLOreceiver('Receiving trial info...');
            %This would be for invalid trials
            if endTrialPacket(1)==2
                disp(videoFilename);
%                 pause(0.5);
%                 %We delete the set of files that was just saved
%                 delete(videoPath);delete(stabCSVPath);delete(stabPath);
            else % valid trials
                waitbar(validTrialCounter/96,f,sprintf('%d/%d',validTrialCounter,96));
%                 fprintf('%d/%d\n',validTrialCounter,96);
                validTrialCounter=validTrialCounter+1;
            end
                
            netcomm('close',SYSPARAMS.netcommobj);
            clear SYSPARAMS.netcommobj;
            SYSPARAMS.netcommobj = 0;

            % we also check if the spacing sequence on the stimulus machine is
            % complete
            if endTrialPacket(2)==1
                % we increment trial number based on very last one
                trialStartNumNextBlock=str2num(trial_num)+1;
                % we break out of the while loop
                break
            end

        end
    end
else
    for repeatIdx=1:size(repeatTrials,1)
        currentTrial=repeatTrials(repeatIdx,:);
        eccentricity=currentTrial(4);
        VideoParams.videodur=60;
        video_length=60;
        % we would only run this once for the each condition
        num_repeats=1;
        %we set the other parameters using the values from currentTrial
        %array
        no_microsaccade_condition=currentTrial(6);flanker_spacing=currentTrial(5);
        subject_id=currentTrial(1);session_num=currentTrial(2);trial_start_counter=currentTrial(3);
        % MESSAGE SEND:
        TSLOsender(eccentricity,no_microsaccade_condition,num_repeats,flanker_spacing...
        ,'Sending experiment Parameters...')
        TSLOsender(subject_id,session_num,trial_start_counter,video_length...
                    ,'Sending subject info...');
        while true
            % we get the flanker spacing condition from the display PC        
            spacing_packet=TSLOreceiver('Receiving Flanker info...');
            no_microsacc_cond=TSLOreceiver('Microsaccade Condition..');
            % we format the spacing value as a string
            spacing_value=num2str(spacing_packet(1));
            trial_num=num2str(spacing_packet(2));
            SOA_value=no_microsacc_cond(2);
            disp(['Trial no.: ',trial_num,':','No Microsaccade condition:',no_microsacc_cond(1)]);
            SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
            mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
            mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
            mess3= ['X#''dummy''#']; % This starts stabilization
            mess4=['Z#''dummy''#']; % This stops  video recording

            netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
            pause(0.001);
            netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
            pause(0.001);
    %         netcomm('write',SYSPARAMS.netcommobj,int8(mess3));
    %         pause(0.001);
            % Get output filenames for video and stabilization files
            timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');
            if no_microsacc_cond(1)==1
                videoFilename=['S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_NoMicrosaccade_Video','_',timeStamp,'.avi'];
            else
                videoFilename=['S0',num2str(subject_id),'_S',...
                            num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                            spacing_value,'_Video','_',timeStamp,'.avi'];
            end
            stabCSV=[videoFilename(1:end-4),'.csv'];
            stabFilename=[videoFilename(1:end-4),'_stabilized','.avi'];
            videoFolder='D:\Video_Folder';
            %Filepaths
            videoPath=[videoFolder,'\',videoFilename];
            stabCSVPath=[videoFolder,'\',stabCSV];
            stabPath=[videoFolder,'\',stabFilename];
            % Start video recording 
            if no_microsacc_cond(1)==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_NoMicrosaccade_Video','_',timeStamp]));
            else
                    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                        num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                        spacing_value,'_Video','_',timeStamp]));
            end
            pause(0.001);  
            %We receive a message regarding whether or not this trial is
            %the last one from the sequence
            endTrialPacket=TSLOreceiver('Receiving trial info...');

            netcomm('close',SYSPARAMS.netcommobj);
            clear SYSPARAMS.netcommobj;
            SYSPARAMS.netcommobj = 0;
            % we also check if the spacing sequence on the stimulus machine is
            % complete
            if endTrialPacket(2)==1
                % we break out of the while loop
                break
            end
        end
    end
end
% indicate to examiner/suzbject that this is the last one
sound(y1,Fs1);
close all;