clc;clear all;

VideoParams.videofolder = 'no video folder';
VideoParams.vidprefix = 'KSP';
VideoParams.videodur = 20;


%KSP: Enter the expt parameters here
subject_id='S01';
trial_start_counter=1;
video_length=VideoParams.videodur;
% TSLOsender(video_length,0,0,0,'Sending experiment Parameters...')

timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');
SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
pause(0.001)
netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
pause(0.001);
netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
pause(0.001);
netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#',num2str(subject_id),'_Video_',timeStamp]));
pause(0.001);
netcomm('close',SYSPARAMS.netcommobj);
clear SYSPARAMS.netcommobj;
SYSPARAMS.netcommobj = 0;
    
