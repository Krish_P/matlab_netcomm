# Purpose: We read the file from ICANDI that contains image parameters & send it to stimulus PC
# author: KSP 2021
import time
from pylsl import StreamInfo, StreamOutlet


while True:
    try:
        info = StreamInfo('Clight', 'ICANDI', 2, 100, 'float32', 'myuid34234')
        # next make an outlet
        outlet = StreamOutlet(info)
        # we wait till we get a receiver
        while True:
            if outlet.have_consumers()==False:
                continue
            else:
                # We open the file and read current set of mean & sd from image
                with(open('D:\\Video_Folder\\image_params.txt')) as f:
                    lines=f.readlines()
                    for aline in lines:
                        splitLine=aline.split(',')
                imgMean=int(splitLine[0])
                imgSD=int(splitLine[1])
                mysample=[imgMean,imgSD]
                outlet.push_sample(mysample)
                time.sleep(0.01)
    except KeyboardInterrupt:
        print('Process closed!')
        break