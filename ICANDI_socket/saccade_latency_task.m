% Purpose: Is to control experiments from the TSLO machine by sending
% messages to ICANDI and also to the stimulus computer using
% labstreaminglayer(LSL) libraries

clc;clear all;
%% Experiment Parameters
% we load the audio file to indicate the completion of video recording
[y,Fs]=audioread('breep.wav');
% another one to indicate the end of the sequence
[y1,Fs1]=audioread('ding.wav');
% We would also have a toggle for running custom trial sequences
% Here we set the experimental conditions and the retinal video recording
% parameters prior to the start of the experiment
% Note: For spacing condition: Setting it to 1 which is also the default
% would result in testing all flanker spacings otherwise when set to 0
% would test the one that is specified locally on the stimulus machine
prompt = {'SubjectID:','Session Number:','Number of repeats:','Video Duration',...
           'Trial start couznter:'};
title = 'Experimental Protocol';
dims = [1 35];
definput = {'1','1','25','2','1'};
response = inputdlg(prompt,title,dims,definput);
ecc_cond=0.33; % We only test at this eccentricity
%% Main experiment
VideoParams.vidprefix='Test';
VideoParams.videofolder = 'no video folder';
VideoParams.videodur = str2num(response{4});
% Here we use the response from the user to set the experimental parameters
% some of which are sent across the stimulus computer through LSL
% The following contain subject info
subject_id=str2num(response{1});
session_num=str2num(response{2});
num_repeats=str2num(response{3});
% We also get the trial start counter from the dialog box
trial_start_counter=str2num(response{5});

%This initiates the loop that initiates video recording on ICANDI and marks
%the start of the experiment
% We have two conditions here:
% 1. Normal trial sequence
eccentricity=ecc_cond;
video_length=VideoParams.videodur;
% MESSAGE SEND:
TSLOsender(subject_id,session_num,num_repeats,video_length,'Sending experiment Parameters...')
for repeatIdx=1:num_repeats
    % This would help synchronise the two systems
    start_message=TSLOreceiver('Waiting for other machine...');
    % we format the spacing value as a string
    trial_num=num2str(start_message(1));
    disp(['Trial no.: ',trial_num]);
    SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
    mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
    mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
    mess3= ['X#''dummy''#']; % This starts stabilization
    mess4=['Z#''dummy''#']; % This stops  video recording

    netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
    pause(0.001);
    netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
    pause(0.001);
%         netcomm('write',SYSPARAMS.netcommobj,int8(mess3));
%         pause(0.001);
    % Get output filenames for video and stabilization files
    timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');

    videoFilename=['S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),timeStamp,'.avi'];
    
    stabCSV=[videoFilename(1:end-4),'.csv'];
    stabFilename=[videoFilename(1:end-4),'_stabilized','.avi'];
    videoFolder='D:\Video_Folder';
    %Filepaths
    videoPath=[videoFolder,'\',videoFilename];
    stabCSVPath=[videoFolder,'\',stabCSV];
    stabPath=[videoFolder,'\',stabFilename];
    % Start video recording 
    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
        num2str(session_num),'_T00',num2str(trial_num),'_Video','_',timeStamp]));
    pause(0.001);  

    netcomm('close',SYSPARAMS.netcommobj);
    clear SYSPARAMS.netcommobj;
    SYSPARAMS.netcommobj = 0;

end
pause(2);
% indicate to examiner/suzbject that this is the last one
sound(y1,Fs1);