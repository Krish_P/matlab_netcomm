%ICANDI Testing
SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
mess1 = ['VL#' num2str(60) '#'];
mess2= ['X#''dummy''#'];
mess3=['Z#''dummy''#'];

netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
pause(0.001);
netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
pause(0.001);
netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S1']));
pause(3);
netcomm('write',SYSPARAMS.netcommobj,int8(mess3));
netcomm('close',SYSPARAMS.netcommobj);
clear SYSPARAMS.netcommobj;
SYSPARAMS.netcommobj = 0;