% Purpose: Is to control experiments from the TSLO machine by sending
% messages to ICANDI and also to the stimulus computer using
% labstreaminglayer(LSL) libraries

clc;clear all;
%% Experiment Parameters
% we load the audio file to indicate the completion of video recording
[y,Fs]=audioread('breep.wav');
% another one to indicate the end of the sequence
[y1,Fs1]=audioread('ding.wav');
% We would also have a toggle for running custom trial sequences
custom_trial=false;
if custom_trial==false
    % Here we set the experimental conditions and the retinal video recording
    % parameters prior to the start of the experiment
    % Note: For spacing condition: Setting it to 1 which is also the default
    % would result in testing all flanker spacings otherwise when set to 0
    % would test the one that is specified locally on the stimulus machine
    prompt = {'SubjectID:','Session Number:','Number of repeats:','Video Duration',...
               'no microsaccade condition:','Trial start counter:'};
    title = 'Experimental Protocol';
    dims = [1 35];
    definput = {'3','1','5','60','0','1'};
    response = inputdlg(prompt,title,dims,definput);
    % Get response from subject to decide which eccentricity conditions to run
    answer = questdlg('What eccentricities would you like to test?', ...
        'Eccentricity Condition', '2 deg','5 deg','Both','Both');
    % Handle response
    switch answer
        case '2 deg'
            ecc_list = [2];
        case '5 deg'
            ecc_list = [5];
        case 'Both'
            ecc_list = Shuffle([2,5]);   
    end
else
    % We manually enter the trials that were found to have issues during
    % analysis
    [file,path,indx] = uigetfile('Z:\TSLO_Experiment\CrowdingStream\RepeatSequenceMatFiles');
    repeatFile=load([path,file]);repeatTrials=repeatFile.repeatTrials;
end
%% Main experiment
VideoParams.vidprefix='Test';
VideoParams.videofolder = 'no video folder';
if custom_trial==false
    VideoParams.videodur = str2num(response{4});
    % Here we use the response from the user to set the experimental parameters
    % some of which are sent across the stimulus computer through LSL
    % The following contain subject info
    subject_id=str2num(response{1});
    session_num=str2num(response{2});
    num_repeats=str2num(response{3});
    % The following are the experimental conditions/parameters
    no_microsaccade_condition=str2num(response{5});
    % We also get the trial start counter from the dialog box
    trial_start_counter=str2num(response{6});
end

%This initiates the loop that initiates video recording on ICANDI and marks
%the start of the experiment
% We have two conditions here:
% 1. Normal trial sequence
% 2. Custom trial sequence
if custom_trial==false
    for ecc_idx=1:length(ecc_list)
        an_ecc=ecc_list(ecc_idx);
        disp(['Current Eccentricity is : ',num2str(an_ecc)]);
        eccentricity=an_ecc;
        video_length=VideoParams.videodur;
        % MESSAGE SEND:
        TSLOsender(eccentricity,no_microsaccade_condition,num_repeats,subject_id...
                    ,'Sending experiment Parameters...')
        TSLOsender(subject_id,session_num,trial_start_counter,video_length...
                    ,'Sending subject info...');
        while true
            % we get the flanker spacing condition from the display PC        
            spacing_packet=TSLOreceiver('Receiving Flanker info...');
            no_microsacc_cond=TSLOreceiver('Microsaccade Condition..');
            % we format the spacing value as a string
            spacing_value=num2str(spacing_packet(1));
            trial_num=num2str(spacing_packet(2));
            disp(['Trial no.: ',trial_num,';','spacing:',spacing_value]);
            SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
            mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
            mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
            mess3= ['X#''dummy''#']; % This starts stabilization
            mess4=['Z#''dummy''#']; % This stops  video recording

            netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
            pause(0.001);
            netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
            pause(0.001);
    %         netcomm('write',SYSPARAMS.netcommobj,int8(mess3));
    %         pause(0.001);
            % Get output filenames for video and stabilization files
            timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');
            if no_microsacc_cond(1)==1
                videoFilename=['S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_NoMicrosaccade_Video','_',timeStamp,'.avi'];
            else
                videoFilename=['S0',num2str(subject_id),'_S',...
                            num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                            spacing_value,'_Video','_',timeStamp,'.avi'];
            end
            stabCSV=[videoFilename(1:end-4),'.csv'];
            stabFilename=[videoFilename(1:end-4),'_stabilized','.avi'];
            videoFolder='D:\Video_Folder';
            %Filepaths
            videoPath=[videoFolder,'\',videoFilename];
            stabCSVPath=[videoFolder,'\',stabCSV];
            stabPath=[videoFolder,'\',stabFilename];
            % Start video recording 
            if no_microsacc_cond(1)==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_NoMicrosaccade_Video','_',timeStamp]));
            else
                    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                        num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                        spacing_value,'_Video','_',timeStamp]));
            end
            pause(0.001);  

            %TODO add tslo receive function and stop stab method to end video
            %recording
            endTrialPacket=TSLOreceiver('Receiving trial info...');

            % we check if the trial has ended 
            if endTrialPacket(1)==1
                %we stop video recording
                netcomm('write',SYSPARAMS.netcommobj,int8(mess4));  
                pause(0.001);
            % This would be for invalid trials
            elseif endTrialPacket(1)==2
                %we stop video recording
                netcomm('write',SYSPARAMS.netcommobj,int8(mess4));  
                pause(0.5);
                %We delete the set of files that was just saved
                delete(videoPath);delete(stabCSVPath);delete(stabPath);
            end
            netcomm('close',SYSPARAMS.netcommobj);
            clear SYSPARAMS.netcommobj;
            SYSPARAMS.netcommobj = 0;
            % we also check if the spacing sequence on the stimulus machine is
            % complete
            if endTrialPacket(2)==1
                % we break out of the while loop
                break
            end
        end
    end
    % indicate to examiner/suzbject that this is the last one
    sound(y1,Fs1);
else
    for repeatIdx=1:size(repeatTrials,1)
        currentTrial=repeatTrials(repeatIdx,:);
        eccentricity=currentTrial(4);
        VideoParams.videodur=60;
        video_length=60;
        % we would only run this once for the each condition
        num_repeats=1;
        %we set the other parameters using the values from currentTrial
        %array
        no_microsaccade_condition=currentTrial(6);flanker_spacing=currentTrial(5);
        subject_id=currentTrial(1);session_num=currentTrial(2);trial_start_counter=currentTrial(3);
        % MESSAGE SEND:
        TSLOsender(eccentricity,no_microsaccade_condition,num_repeats,flanker_spacing...
        ,'Sending experiment Parameters...')
        TSLOsender(subject_id,session_num,trial_start_counter,video_length...
                    ,'Sending subject info...');
        while true
            % we get the flanker spacing condition from the display PC        
            spacing_packet=TSLOreceiver('Receiving Flanker info...');
            no_microsacc_cond=TSLOreceiver('Microsaccade Condition..');
            % we format the spacing value as a string
            spacing_value=num2str(spacing_packet(1));
            trial_num=num2str(spacing_packet(2));
            disp(['Trial no.: ',trial_num,':','No Microsaccade condition:',no_microsacc_cond(1)]);
            SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
            mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
            mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
            mess3= ['X#''dummy''#']; % This starts stabilization
            mess4=['Z#''dummy''#']; % This stops  video recording

            netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
            pause(0.001);
            netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
            pause(0.001);
    %         netcomm('write',SYSPARAMS.netcommobj,int8(mess3));
    %         pause(0.001);
            % Get output filenames for video and stabilization files
            timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');
            if no_microsacc_cond(1)==1
                videoFilename=['S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_NoMicrosaccade_Video','_',timeStamp,'.avi'];
            else
                videoFilename=['S0',num2str(subject_id),'_S',...
                            num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                            spacing_value,'_Video','_',timeStamp,'.avi'];
            end
            stabCSV=[videoFilename(1:end-4),'.csv'];
            stabFilename=[videoFilename(1:end-4),'_stabilized','.avi'];
            videoFolder='D:\Video_Folder';
            %Filepaths
            videoPath=[videoFolder,'\',videoFilename];
            stabCSVPath=[videoFolder,'\',stabCSV];
            stabPath=[videoFolder,'\',stabFilename];
            % Start video recording 
            if no_microsacc_cond(1)==1
                netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                spacing_value,'_NoMicrosaccade_Video','_',timeStamp]));
            else
                    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',...
                        num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_spacing_',...
                        spacing_value,'_Video','_',timeStamp]));
            end
            pause(0.001);  

            %TODO add tslo receive function and stop stab method to end video
            %recording
            endTrialPacket=TSLOreceiver('Receiving trial info...');

            % we check if the trial has ended 
            if endTrialPacket(1)==1
                %we stop video recording
                netcomm('write',SYSPARAMS.netcommobj,int8(mess4));  
                pause(0.001);
            % This would be for invalid trials
            elseif endTrialPacket(1)==2
                %we stop video recording
                netcomm('write',SYSPARAMS.netcommobj,int8(mess4));  
                pause(0.5);
                %We delete the set of files that was just saved
                delete(videoPath);delete(stabCSVPath);delete(stabPath);
            end
            netcomm('close',SYSPARAMS.netcommobj);
            clear SYSPARAMS.netcommobj;
            SYSPARAMS.netcommobj = 0;
            % we also check if the spacing sequence on the stimulus machine is
            % complete
            if endTrialPacket(2)==1
                % we break out of the while loop
                break
            end
        end
    end
end
