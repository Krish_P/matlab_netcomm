clc;clear all;

VideoParams.videofolder = 'no video folder';
VideoParams.vidprefix = 'KSP';
VideoParams.videodur = 2;


%KSP: Enter the expt parameters here
subject_id=2;
session_num=12;
ecc_deg=3;
spacing_cond=0; %1 for all or specify spacing
cue_cond=0;
trial_start_counter=1;
video_length=VideoParams.videodur;
TSLOsender(ecc_deg,spacing_cond,cue_cond,subject_id...
            ,'Sending experiment Parameters...')
TSLOsender(subject_id,session_num,trial_start_counter,video_length...
            ,'Sending subject info...');
for i=1:50
    % we get the flanker spacing condition from the display PC        
    stimulus_machine_packet=TSLOreceiver('Receiving info from stimulus machine...');
    % we format the spacing value as a string
    spacing_value=num2str(stimulus_machine_packet(1));
    trial_num=num2str(stimulus_machine_packet(2));
    % Get output filenames for video and stabilization files
    timeStamp=datestr(now,'mm_dd_yyyy_HH_MM_SS');
    SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
    mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
    mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
    pause(0.001)
    netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
    pause(0.001);
    netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
    pause(0.001);
    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(ecc_deg),'_spacing_',num2str(spacing_value),'_Video_',timeStamp]));
    tStart = tic;  
    pause(0.001);
    netcomm('close',SYSPARAMS.netcommobj);
    clear SYSPARAMS.netcommobj;
    SYSPARAMS.netcommobj = 0;
    
    while true
        tEnd = toc(tStart);
        if tEnd>2.5
            break
        end
    end

end
