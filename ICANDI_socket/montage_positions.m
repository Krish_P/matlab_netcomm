clc;clear all;

VideoParams.videofolder = 'no video folder';
VideoParams.vidprefix = 'KSP';
VideoParams.videodur = 30;

%KSP: Enter the expt parameters here
subject_id=2;
session_num=1;
trial_num=1;
ecc_deg=1;
spacing_cond=1; %1 for all or specify spacing
cue_cond=0;
video_length=VideoParams.videodur;
TSLOsender(subject_id,session_num,trial_num,video_length...
            ,'Sending recording info...');

SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor

netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
pause(0.001);
netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
pause(0.001);
netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_montage']));


netcomm('close',SYSPARAMS.netcommobj);
clear SYSPARAMS.netcommobj;
SYSPARAMS.netcommobj = 0;

