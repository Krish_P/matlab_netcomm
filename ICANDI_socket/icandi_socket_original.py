import socket
import time

TCP_IP = '127.0.0.1'
TCP_PORT = 1300

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

mess1='VL#2.00#'
mess2='GRVIDT#test_rec_1#'

m1=bytearray()
m1.extend(map(ord,mess1))

m2=bytearray()
m2.extend(map(ord,mess2))

s.send(m1)
time.sleep(0.02)
s.send(m2)
time.sleep(0.01)

#time.sleep(1.0)

s.close()
