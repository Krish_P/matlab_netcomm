function [sample]=TSLOreceiver(inputMessage)
    %% instantiate the library
    lib = lsl_loadlib();

    % resolve a stream...
    result = {};
    while isempty(result)
        result = lsl_resolve_byprop(lib,'type','TSLO'); end

    % create a new inlet
    inlet = lsl_inlet(result{1});

    disp(inputMessage);
    keepGoing=true;
    while keepGoing==true
        % get data from the inlet
        [sample,ts] = inlet.pull_sample();
        %stop after we get the first sample
        if length(sample)>1
            keepGoing=false;
            inlet.close_stream()
        end
    end
end