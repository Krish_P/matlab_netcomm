% Purpose: Is to control experiments from the TSLO machine by sending
% messages to ICANDI and also to the stimulus computer using
% labstreaminglayer(LSL) libraries

clc;clear all;
%% Experiment Parameters
% Here we set the experimental conditions and the retinal video recording
% parameters prior to the start of the experiment
% Note: For spacing condition: Setting it to 1 which is also the default
% would result in testing all flanker spacings otherwise when set to 0
% would test the one that is specified locally on the stimulus machine
prompt = {'SubjectID:','Session Number:','Trial Num:','Video Duration',...
           'Flanker Condition:','Cue Condition:'};
title = 'Experimental Protocol';
dims = [1 35];
definput = {'4','1','1','120','1','0'};
response = inputdlg(prompt,title,dims,definput);
% Note: Ecc list would have be changed on each run
ecc_list=Shuffle([2]);
disp(ecc_list);
%% Main experiment
VideoParams.vidprefix='Test';
VideoParams.videofolder = 'no video folder';
VideoParams.videodur = str2num(response{4});

% Here we use the response from the user to set the experimental parameters
% some of which are sent across the stimulus computer through LSL
% The following contain subject info
subject_id=str2num(response{1});
session_num=str2num(response{2});
trial_num=str2num(response{3});
% The following are the experimental conditions/parameters
flanker_condition=str2num(response{5});
cue_condition=str2num(response{6});

%This initiates the loop that initiates video recording on ICANDI and marks
%the start of the experiment
for idx=1:length(ecc_list)
    an_ecc=ecc_list(idx);
    disp(an_ecc)
    eccentricity=an_ecc;
    video_length=VideoParams.videodur;
    % MESSAGE SEND:
    TSLOsender(eccentricity,flanker_condition,cue_condition,subject_id...
            ,'Sending experiment Parameters...')
    TSLOsender(subject_id,session_num,trial_num,video_length...
                ,'Sending subject info...');

    SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
    mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
    mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor
    
    netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
    pause(0.001);
    netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
    pause(0.001);
    netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),'_S',num2str(session_num),'_T00',num2str(trial_num),'_ecc_',num2str(eccentricity),'_Video_',datestr(now,'DDMMYYYY')]));

    netcomm('close',SYSPARAMS.netcommobj);
    clear SYSPARAMS.netcommobj;
    SYSPARAMS.netcommobj = 0;
end
% indicate to examiner/subject that this is the last one
beep;pause(0.5);beep;
