%Example program to demonstrate how to send a multi-channel time series to
%LSL.


function [varargout]=TSLOsender(param1,param2,param3,param4,msg)

% first create a new stream info (here we set the name to BioSemi,
% the content-type to EEG, 8 channels, 100 Hz, and float-valued data) The
% last value would be the serial number of the device or some other more or
% less locally unique identifier for the stream as far as available (you
% could also omit it but interrupted connections wouldn't auto-recover)
%KSP: params:Format [SubjectID,SessionNum,TrialNum,RecordingDuration]
    % instantiate the library
%     disp('Loading library...');
    lib = lsl_loadlib();

    % make a new stream outlet
%     disp('Creating a new streaminfo...');
    info = lsl_streaminfo(lib,'Clight','TSLO',4,100,'cf_float32','sdfwerr32432');

%     disp('Opening an outlet...');
    outlet = lsl_outlet(info);

    counter=0;
    
    % The parameters specified here would be sent across to the Stimulus
    % computer from the TSLO computer
    params=[param1,param2,param3,param4];

    repeat=1;
    disp(msg);
    while repeat==1
        if counter>1
            repeat=0;
        end
        if outlet.have_consumers()==0
            continue
        end
        mysample = params;
        % now send it and wait for a bit       
        outlet.push_sample(mysample);
        counter=counter+1;
        pause(0.01);
    end
