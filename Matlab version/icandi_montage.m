% Purpose: Is to control the sequence of retinal video recordings during 
%the montaging session using labstreaminglayer(LSL) libraries

clc;clear all;
%% Experiment Parameters
% Here we set the experimental conditions and the retinal video recording
% parameters prior to the start of the montaging session
prompt = {'SubjectID:','Video Duration(s):','Distance from fixation (degrees):'...
            ,'Horizontal Res:','Vertical Res:','Diagonal Size:','Distance to screen:'...
            ,'Num Location:'};
title = 'Montage settings';
dims = [1 35];
definput = {'1','5','3','1920','1080','62.5','180','9'};
response = inputdlg(prompt,title,dims,definput);

%Have another dialogue box that gets the ICANDI operation status. Which
%would be used to determine whether or not to send the ICANDI information
answer = questdlg('Is ICANDI connected?','Question?',...
                  'Yes','No','Yes');
% Handle response
switch answer
    case 'Yes'
        ICANDI=true;
    case 'Cake'
        ICANDI=false;
end

%% Montage sequence
% We use the input from the user to create a set of target locations that
% would be used by the stimulus computer to position the fixation targets
subject_id=response{1};
dist_fixation_deg=str2num(response{3});

%setup parameters
horz_res=str2num(response{4});
vert_res=str2num(response{5});
diagonal_size=str2num(response{6}); %diagonal screen size
dist_to_screen=str2num(response{7}); %cm 
%Note for the mirror setup:
%1. Mirror to screen distance: 165 cm
%2. Between cold mirror and big mirror: 10 cm
%3. Mirror to eye distance: 5 cm
pixel_per_degree=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen);
dist_fixation_pixels=dist_fixation_deg*pixel_per_degree;

% We use the parameters provided to form the sequence of fixation targets
% for the montaging sequence
x_min=(-dist_fixation_pixels/2); %left
x_max=(dist_fixation_pixels/2);  %right 
y_min=(dist_fixation_pixels/2);  %top
y_max=(-dist_fixation_pixels/2); %bottom

% MESSAGE SEND: Via LSL to the stimulus machine
TSLOsender(x_min,x_max,y_min,y_max...
            ,'Sending experiment Parameters...')
        
% Next we set up the parameters for the video recording 
VideoParams.videodur = str2num(response{2});
num_locations=str2num(response{8});
montage_pos_list=['center','left','right','top','top_left','top_right',...
                    'bottom','bottom_left','bottom_right'];

for test_loc=1:num_locations
    montage_loc=montage_pos_list(test_loc);
    
    %Have TSLO message here to sync the two systems. The TSLOSender
    %currently expects 4 params so here we have dummy messages (0's) to
    %keep it consistent
    TSLOsender(VideoParams.videodur,test_loc,0,0,'Starting expt...');
    
    %messages to be sent to ICANDI to start recording
    if ICANDI==true
        SYSPARAMS.netcommobj = netcomm('REQUEST', '127.0.0.1', 1300, 'timeout', 5000);
        mess1 = ['VP#' VideoParams.vidprefix '#']; %#ok<NASGU>
        mess2 = ['VL#' num2str(VideoParams.videodur) '#']; %#ok<NASGU> %KSP Not sure of the +1 factor

        netcomm('write',SYSPARAMS.netcommobj,int8(mess1));
        pause(0.001);
        netcomm('write',SYSPARAMS.netcommobj,int8(mess2));
        pause(0.001);
        netcomm('write',SYSPARAMS.netcommobj,int8(['GRVIDT#S0',num2str(subject_id),montage_loc,'_Video_',datestr(now,'DDMMYYYY')]));


        netcomm('close',SYSPARAMS.netcommobj);
        clear SYSPARAMS.netcommobj;
        SYSPARAMS.netcommobj = 0; 
    end
end
%% Pixel to degree function
% This is used to convert degrees in visual angle to pixels on the pixels
% on the stimulus computer using the specifications provided
function [pixels_per_degree]=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)
    diagonal_pixel=sqrt((horz_res^2)+(vert_res^2));
    pixel_per_cm=diagonal_pixel/diagonal_size;
    
    %visual angle calcualtion: we use the above info to calc the number of
    %pixels that fall within a given degrees in visual angle
    cm_per_degree=(dist_to_screen*pi)/180;
    pixels_per_degree=cm_per_degree*pixel_per_cm;
    
end